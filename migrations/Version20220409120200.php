<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220409120200 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE calendar (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, main_group_id INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', title VARCHAR(255) NOT NULL, archive TINYINT(1) DEFAULT 0 NOT NULL, need_club_access TINYINT(1) DEFAULT 0 NOT NULL, need_group_administration TINYINT(1) DEFAULT 0 NOT NULL, is_public TINYINT(1) DEFAULT 0 NOT NULL, event_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', content LONGTEXT NOT NULL, INDEX IDX_6EA9A14661220EA6 (creator_id), INDEX IDX_6EA9A1462776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE calendar_group (calendar_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_19447ABA40A2C8 (calendar_id), INDEX IDX_19447ABFE54D947 (group_id), PRIMARY KEY(calendar_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `group` (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', short_name VARCHAR(12) NOT NULL, motd LONGTEXT DEFAULT NULL, motd_updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news (id INT AUTO_INCREMENT NOT NULL, creator_id INT NOT NULL, main_group_id INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', title VARCHAR(255) NOT NULL, archive TINYINT(1) DEFAULT 0 NOT NULL, need_club_access TINYINT(1) DEFAULT 0 NOT NULL, need_group_administration TINYINT(1) DEFAULT 0 NOT NULL, is_public TINYINT(1) DEFAULT 0 NOT NULL, content LONGTEXT NOT NULL, INDEX IDX_1DD3995061220EA6 (creator_id), INDEX IDX_1DD399502776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE news_group (news_id INT NOT NULL, group_id INT NOT NULL, INDEX IDX_177FC39BB5A459A0 (news_id), INDEX IDX_177FC39BFE54D947 (group_id), PRIMARY KEY(news_id, group_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE notification (id INT AUTO_INCREMENT NOT NULL, sender_id INT DEFAULT NULL, receiver_id INT NOT NULL, icon VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', readed TINYINT(1) DEFAULT 0 NOT NULL, INDEX IDX_BF5476CAF624B39D (sender_id), INDEX IDX_BF5476CACD53EDB6 (receiver_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE rank (id INT AUTO_INCREMENT NOT NULL, main_group_id INT NOT NULL, name VARCHAR(255) NOT NULL, shortname VARCHAR(255) NOT NULL, power INT NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', permissions JSON DEFAULT NULL, INDEX IDX_8879E8E52776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_group (id INT AUTO_INCREMENT NOT NULL, main_group_id INT NOT NULL, name VARCHAR(255) NOT NULL, short_name VARCHAR(12) DEFAULT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_8184207C2776C832 (main_group_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_group_user (sub_group_id INT NOT NULL, user_id INT NOT NULL, INDEX IDX_88021EF444FB371E (sub_group_id), INDEX IDX_88021EF4A76ED395 (user_id), PRIMARY KEY(sub_group_id, user_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_group_news (sub_group_id INT NOT NULL, news_id INT NOT NULL, INDEX IDX_184251ED44FB371E (sub_group_id), INDEX IDX_184251EDB5A459A0 (news_id), PRIMARY KEY(sub_group_id, news_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sub_group_calendar (sub_group_id INT NOT NULL, calendar_id INT NOT NULL, INDEX IDX_63DBF2D44FB371E (sub_group_id), INDEX IDX_63DBF2DA40A2C8 (calendar_id), PRIMARY KEY(sub_group_id, calendar_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE calendar ADD CONSTRAINT FK_6EA9A14661220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE calendar ADD CONSTRAINT FK_6EA9A1462776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE calendar_group ADD CONSTRAINT FK_19447ABA40A2C8 FOREIGN KEY (calendar_id) REFERENCES calendar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE calendar_group ADD CONSTRAINT FK_19447ABFE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD3995061220EA6 FOREIGN KEY (creator_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE news ADD CONSTRAINT FK_1DD399502776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE news_group ADD CONSTRAINT FK_177FC39BB5A459A0 FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE news_group ADD CONSTRAINT FK_177FC39BFE54D947 FOREIGN KEY (group_id) REFERENCES `group` (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CAF624B39D FOREIGN KEY (sender_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification ADD CONSTRAINT FK_BF5476CACD53EDB6 FOREIGN KEY (receiver_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE rank ADD CONSTRAINT FK_8879E8E52776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE sub_group ADD CONSTRAINT FK_8184207C2776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE sub_group_user ADD CONSTRAINT FK_88021EF444FB371E FOREIGN KEY (sub_group_id) REFERENCES sub_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sub_group_user ADD CONSTRAINT FK_88021EF4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sub_group_news ADD CONSTRAINT FK_184251ED44FB371E FOREIGN KEY (sub_group_id) REFERENCES sub_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sub_group_news ADD CONSTRAINT FK_184251EDB5A459A0 FOREIGN KEY (news_id) REFERENCES news (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sub_group_calendar ADD CONSTRAINT FK_63DBF2D44FB371E FOREIGN KEY (sub_group_id) REFERENCES sub_group (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sub_group_calendar ADD CONSTRAINT FK_63DBF2DA40A2C8 FOREIGN KEY (calendar_id) REFERENCES calendar (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user ADD main_group_id INT DEFAULT NULL, ADD main_rank_id INT DEFAULT NULL, ADD admin_mode TINYINT(1) DEFAULT 0 NOT NULL, ADD firstname VARCHAR(255) NOT NULL, ADD lastname VARCHAR(255) NOT NULL, ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD is_desactivated TINYINT(1) DEFAULT 0 NOT NULL, ADD locale VARCHAR(255) DEFAULT NULL, CHANGE is_verified is_verified TINYINT(1) DEFAULT 0 NOT NULL');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D6492776C832 FOREIGN KEY (main_group_id) REFERENCES `group` (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649825DCE66 FOREIGN KEY (main_rank_id) REFERENCES rank (id)');
        $this->addSql('CREATE INDEX IDX_8D93D6492776C832 ON user (main_group_id)');
        $this->addSql('CREATE INDEX IDX_8D93D649825DCE66 ON user (main_rank_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE calendar_group DROP FOREIGN KEY FK_19447ABA40A2C8');
        $this->addSql('ALTER TABLE sub_group_calendar DROP FOREIGN KEY FK_63DBF2DA40A2C8');
        $this->addSql('ALTER TABLE calendar DROP FOREIGN KEY FK_6EA9A1462776C832');
        $this->addSql('ALTER TABLE calendar_group DROP FOREIGN KEY FK_19447ABFE54D947');
        $this->addSql('ALTER TABLE news DROP FOREIGN KEY FK_1DD399502776C832');
        $this->addSql('ALTER TABLE news_group DROP FOREIGN KEY FK_177FC39BFE54D947');
        $this->addSql('ALTER TABLE rank DROP FOREIGN KEY FK_8879E8E52776C832');
        $this->addSql('ALTER TABLE sub_group DROP FOREIGN KEY FK_8184207C2776C832');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D6492776C832');
        $this->addSql('ALTER TABLE news_group DROP FOREIGN KEY FK_177FC39BB5A459A0');
        $this->addSql('ALTER TABLE sub_group_news DROP FOREIGN KEY FK_184251EDB5A459A0');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649825DCE66');
        $this->addSql('ALTER TABLE sub_group_user DROP FOREIGN KEY FK_88021EF444FB371E');
        $this->addSql('ALTER TABLE sub_group_news DROP FOREIGN KEY FK_184251ED44FB371E');
        $this->addSql('ALTER TABLE sub_group_calendar DROP FOREIGN KEY FK_63DBF2D44FB371E');
        $this->addSql('DROP TABLE calendar');
        $this->addSql('DROP TABLE calendar_group');
        $this->addSql('DROP TABLE `group`');
        $this->addSql('DROP TABLE news');
        $this->addSql('DROP TABLE news_group');
        $this->addSql('DROP TABLE notification');
        $this->addSql('DROP TABLE rank');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE sub_group');
        $this->addSql('DROP TABLE sub_group_user');
        $this->addSql('DROP TABLE sub_group_news');
        $this->addSql('DROP TABLE sub_group_calendar');
        $this->addSql('DROP INDEX IDX_8D93D6492776C832 ON user');
        $this->addSql('DROP INDEX IDX_8D93D649825DCE66 ON user');
        $this->addSql('ALTER TABLE user DROP main_group_id, DROP main_rank_id, DROP admin_mode, DROP firstname, DROP lastname, DROP created_at, DROP is_desactivated, DROP locale, CHANGE is_verified is_verified TINYINT(1) NOT NULL');
    }
}
