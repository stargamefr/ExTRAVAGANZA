  
# ExTRAVAGANZA

[![pipeline status](https://gitlab.com/stargamefr/ExTRAVAGANZA/badges/main/pipeline.svg)](https://gitlab.com/stargamefr/ExTRAVAGANZA/-/commits/main)

This is ExTRAVAGANZA Night Club web site to allow  to have a Personalized Calendar for ExTRAVAGANZA

## Requirements

- Php ^8.0
- Php extensions : bz2, curl, fileinfo, gd, intl, mbstring, mysqli, openssl, pdo_mysql, xsl
- Git (easier way to install and update)
- Composer (<https://getcomposer.org/download/>)
- An SQL server
- A mail server (or a smtp to send mail)
- Donuts (because cops will use this panel)  

## Installation

1. Clone the repo with the tool of your choice

2. Enter the folder created

3. Copy the "dotEnvDemo" file to ".env", and set all required informations

4. Install all dependencies with composer

    ```bash
    composer install --no-scripts
    ```

5. Setup the database with symfony commands

    > info: If you use the symfony cli, you can replace "php bin/console" by
    > "symfony console"

    ```bash
    php bin/console doctrine:database:create
    php bin/console doctrine:migrations:migrate
    ```

    > info: if you start your database with docker-compose, the database is
    > already created, so ignore the "doctrine:database:create" command

6. Add a new group to ExTRAVAGANZA

    ```bash
    php bin/console extravaganza:addgroup
    ```

7. Add a new rank to this new group

    ```bash
    php bin/console extravaganza:addrank
    ```
  
8. Add a new user to extravaganza, and choose group and rank

    ```bash
    php bin/console extravaganza:adduser
    ```

    info : the user is created with a random password, so at first time, you need to reset your password before login
    info : If this is your admin account; dont forget to answer yes while asking if user is admin

9. Start extravaganza the way you want ! 

    with symfony cli:

    ```bash
    symfony serve
    ```

    or with php dev server :

    ```bash
    php -S localhost:8000 -t public
    ```

    and navigate to localhost:8000 with your web browser

## Update  

1. Enter the folder containing ExTRAVAGANZA

2. update by pulling the repo

    ```bash
    git pull
    ```

3. update the database with symfony commands  

    ```bash
    php bin/console doctrine:migrations:migrate
    ```

4. clear cache

    ```bash
    php bin/console cache:clear
    ```

## Scalability

ExTRAVAGANZA is ready to be used in scaled architecture :

- user's sessions are stored in database
- just share the "public\uploads" folder between ExTRAVAGANZAs instances to get uploaded files accross instances

## Suggest an idea

> [Open an Enhancement issue](https://gitlab.com/stargamefr/ExTRAVAGANZA/-/issues/new?issuable_template=Enhancement) to improve something already existing
or > [Open an Suggestion issue](https://gitlab.com/stargamefr/ExTRAVAGANZA/-/issues/new?issuable_template=Suggestion) to suggest a new idea

## Report a bug

You found something wrong on ExTRAVAGANZA ?

> [Open an a bug issue ! click here](https://gitlab.com/stargamefr/ExTRAVAGANZA/-/issues/new?issuable_template=Bug)

## Contributing  

Contributions are always welcome!  

Start by installing ExTRAVAGANZA on your computer, then create a .env.local file with  

```env
APP_ENV=dev
```

Then, our way to do it :

1. install docker and docker compose on your computer  

    <https://www.docker.com>

    (example: <https://www.docker.com/products/docker-desktop>)

2. install symfony cli on your computer

    <https://symfony.com/download>  

3. use docker-compose to run a mysql server and MailDev

    ```bash
    docker-compose up -d
    ```

4. Setup database : read #5 on installation part
    you can also do the #6 - #7 to create groups and ranks, or use inclued fixtures :

    ```bash
    symfony console d:f:l
    ```

5. Create your first Admin user : read #8 on installation part

6. start the symfony web server

    ```bash
    symfony serve
    ```

    with docker and symfony serve, symfony app will communicate the best way with docker

## Base idea by  

- [@Djbrown184](https://twitter.com/djbrown184)