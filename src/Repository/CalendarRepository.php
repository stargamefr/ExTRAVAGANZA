<?php

namespace App\Repository;

use App\Entity\Calendar;
use App\Entity\User;
use App\Entity\Group;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Calendar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Calendar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Calendar[]    findAll()
 * @method Calendar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CalendarRepository extends ServiceEntityRepository
{
    public array $fields = [];
    private QueryBuilder $qb;
    private ?QueryBuilder $qbsearch = null;
    private ?Group $group = null;
    private ?User $user = null;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Calendar::class);
    }

    /**
     * Equivalent of FindAll()
     *     *
     * @return QueryBuilder
     */
    public function getQueryBuilder()
    {
        return $this->createQueryBuilder("d");
        ;
    }

    /**
     * Allow listing of documents wich are allowed for a given group
     *
     * @param Group $Group
     * @return QueryBuilder
     */
    public function getGroupQuery(Group $Group)
    {
        return $this->getQueryBuilder()
            ->where(':group MEMBER OF d.allowedGroups')
            ->setParameter('group', $Group)
        ;
    }

    public function order(array $order)
    {
        foreach ($order as $key => $value) {
            if ($key === array_key_first($order)) {
                $this->qb->orderBy('d.' . $key, $value);
            } else {
                $this->qb->addorderBy('d.' . $key, $value);
            }
        }
        return $this;
    }

    public function listForUser(User $User, bool $ignoreGroup = false)
    {
        $this->user = $User;
        $this->group = $User->getMainGroup();
        $this->qbsearch = null;
        $this->qb = ($User->getAdminMode() || $ignoreGroup) ? $this->getQueryBuilder()
                                                            : $this->getGroupQuery($this->group);

        if (!$User->hasPermission('group_ignore_subgroups')) {
            $this->qb->andWhere('d.allowedSubGroups is empty');
            foreach ($User->getSubGroups() as $key => $subG) {
                $this->qb->orWhere(':group' . $key . ' MEMBER OF d.allowedSubGroups')
                ->setParameter(':group' . $key, $subG);
            }
        }

        if (!$User->getAdminMode()) {
            if (!$User->hasPermission('general_club_view')) {
                $this->qb->andWhere('d.needClubAccess = 0');
            }

            if (!$User->hasPermission('group_administrate')) {
                $this->qb->andWhere('d.needGroupAdministration = 0');
            }
        }

        return $this;
    }

    public function list()
    {
        $this->qbsearch = null;
        $this->qb = $this->getQueryBuilder();
        return $this;
    }
    
    public function getResult()
    {
        if (null !== $this->qbsearch) {
            $this->qbsearch->andWhere($this->qb->expr()->in('s.id', $this->qb->getDQL()));
            if (null !== $this->user && !$this->user->getAdminMode()) {
                $this->qbsearch->setParameter('group', $this->group);
            }
            return $this->qbsearch->getQuery()->getResult();
        }

        return $this->qb->getQuery()->getResult();
    }
    
    public function limit(int $limit)
    {
        $this->qb->setMaxResults($limit);
        return $this;
    }
}
