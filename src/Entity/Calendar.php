<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\CalendarRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

#[ORM\Entity(repositoryClass: CalendarRepository::class)]
#[Gedmo\Loggable()]
class Calendar
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'calendars')]
    #[ORM\JoinColumn(nullable: false)]
    private $creator;

    #[ORM\ManyToOne(targetEntity: Group::class, inversedBy: 'calendars')]
    #[ORM\JoinColumn(nullable: false)]
    private $mainGroup;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Gedmo\Timestampable(on: "create")]
    private $createdAt;

    #[ORM\Column(type: 'string', length: 255)]
    #[Gedmo\Versioned]
    private $title;

    #[ORM\ManyToMany(targetEntity: Group::class, inversedBy: 'allowedCalendars')]
    private $allowedGroups;

    #[ORM\Column(type: 'boolean', options: ["default" => "0"])]
    #[Gedmo\Versioned]
    private $archive = false;

    #[ORM\Column(type: 'boolean', options: ["default" => "0"])]
    private $needClubAccess = false;

    #[ORM\Column(type: 'boolean', options: ["default" => "0"])]
    private $needGroupAdministration = false;

    #[ORM\Column(type: 'boolean', options: ["default" => "0"])]
    private $isPublic = false;

    #[ORM\Column(type: 'datetime_immutable')]
    private $eventAt;

    #[ORM\Column(type: 'text')]
    private $content;

    #[ORM\ManyToMany(targetEntity: SubGroup::class, mappedBy: 'calendars')]
    private $allowedSubGroups;

    public function __construct()
    {
        $this->allowedGroups = new ArrayCollection();
        $this->allowedSubGroups = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    public function getMainGroup(): ?Group
    {
        return $this->mainGroup;
    }

    public function setMainGroup(?Group $mainGroup): self
    {
        $this->mainGroup = $mainGroup;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return Collection<int, Group>
     */
    public function getAllowedGroups(): Collection
    {
        return $this->allowedGroups;
    }

    public function addAllowedGroup(Group $allowedGroup): self
    {
        if (!$this->allowedGroups->contains($allowedGroup)) {
            $this->allowedGroups[] = $allowedGroup;
        }

        return $this;
    }

    public function removeAllowedGroup(Group $allowedGroup): self
    {
        $this->allowedGroups->removeElement($allowedGroup);

        return $this;
    }

    public function getArchive(): ?bool
    {
        return $this->archive;
    }

    public function setArchive(bool $archive): self
    {
        $this->archive = $archive;

        return $this;
    }

    public function getNeedClubAccess(): ?bool
    {
        return $this->needClubAccess;
    }

    public function setNeedClubAccess(bool $needClubAccess): self
    {
        $this->needClubAccess = $needClubAccess;

        return $this;
    }

    public function getNeedGroupAdministration(): ?bool
    {
        return $this->needGroupAdministration;
    }

    public function setNeedGroupAdministration(bool $needGroupAdministration): self
    {
        $this->needGroupAdministration = $needGroupAdministration;

        return $this;
    }

    public function getIsPublic(): ?bool
    {
        return $this->isPublic;
    }

    public function setIsPublic(bool $isPublic): self
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    public function getEventAt(): ?\DateTimeImmutable
    {
        return $this->eventAt;
    }

    public function setEventAt(\DateTimeImmutable $eventAt): self
    {
        $this->eventAt = $eventAt;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection<int, SubGroup>
     */
    public function getAllowedSubGroups(): Collection
    {
        return $this->allowedSubGroups;
    }

    public function addAllowedSubGroup(SubGroup $allowedSubGroup): self
    {
        if (!$this->allowedSubGroups->contains($allowedSubGroup)) {
            $this->allowedSubGroups[] = $allowedSubGroup;
            $allowedSubGroup->addCalendar($this);
        }

        return $this;
    }

    public function removeAllowedSubGroup(SubGroup $allowedSubGroup): self
    {
        if ($this->allowedSubGroups->removeElement($allowedSubGroup)) {
            $allowedSubGroup->removeCalendar($this);
        }

        return $this;
    }
}
