<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GroupRepository;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

#[ORM\Entity(repositoryClass: GroupRepository::class)]
#[ORM\Table(name: '`group`')]
#[Gedmo\Loggable()]
class Group
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 255)]
    #[Gedmo\Versioned]
    private $name;

    #[ORM\Column(type: 'datetime_immutable')]
    #[Gedmo\Timestampable(on: "create")]
    private $createdAt;

    #[ORM\OneToMany(mappedBy: 'mainGroup', targetEntity: News::class)]
    private $news;

    #[ORM\ManyToMany(targetEntity: News::class, mappedBy: 'allowedGroups')]
    private $allowedNews;

    #[ORM\OneToMany(mappedBy: 'mainGroup', targetEntity: Calendar::class)]
    private $calendars;

    #[ORM\ManyToMany(targetEntity: Calendar::class, mappedBy: 'allowedGroups')]
    private $allowedCalendars;

    #[ORM\OneToMany(mappedBy: 'mainGroup', targetEntity: User::class)]
    private $users;

    #[ORM\OneToMany(mappedBy: 'mainGroup', targetEntity: Rank::class)]
    private $ranks;

    #[ORM\Column(type: 'string', length: 12)]
    #[Gedmo\Versioned]
    private $shortName;

    #[ORM\Column(type: 'text', nullable: true)]
    #[Gedmo\Versioned]
    private $motd;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Gedmo\Timestampable(on: "change", field: "motd")]
    private $motdUpdatedAt;

    /**
     * @Vich\UploadableField(mapping = "mapping_name", fileNameProperty = "imageName", size = "imageSize")
     *
     * @var File|null
     *
     */
    private $imageFile;
    
    #[ORM\Column(type: 'string', nullable: true)]
    #[Gedmo\Versioned]
    private $imageName;
    
    #[ORM\Column(type: 'integer', nullable: true)]
    private $imageSize;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[Gedmo\Timestampable(on: "update")]
    private $updatedAt;

    #[ORM\OneToMany(mappedBy: 'mainGroup', targetEntity: SubGroup::class)]
    private $subGroups;

    public function __construct()
    {
        $this->news = new ArrayCollection();
        $this->calendars = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->ranks = new ArrayCollection();
        $this->allowedNews = new ArrayCollection();
        $this->allowedCalendars = new ArrayCollection();
        $this->subGroups = new ArrayCollection();
    }

    /**
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeImmutable $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection<int, News>
     */
    public function getNews(): Collection
    {
        return $this->news;
    }

    public function addNews(News $news): self
    {
        if (!$this->news->contains($news)) {
            $this->news[] = $news;
            $news->setMainGroup($this);
        }

        return $this;
    }

    public function removeNews(News $news): self
    {
        if ($this->news->removeElement($news)) {
            // set the owning side to null (unless already changed)
            if ($news->getMainGroup() === $this) {
                $news->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setMainGroup($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getMainGroup() === $this) {
                $user->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Rank>
     */
    public function getRanks(): Collection
    {
        return $this->ranks;
    }

    public function addRank(Rank $rank): self
    {
        if (!$this->ranks->contains($rank)) {
            $this->ranks[] = $rank;
            $rank->setMainGroup($this);
        }

        return $this;
    }

    public function removeRank(Rank $rank): self
    {
        if ($this->ranks->removeElement($rank)) {
            // set the owning side to null (unless already changed)
            if ($rank->getMainGroup() === $this) {
                $rank->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, News>
     */
    public function getAllowedNews(): Collection
    {
        return $this->allowedNews;
    }

    public function addAllowedNews(News $allowedNews): self
    {
        if (!$this->allowedNews->contains($allowedNews)) {
            $this->allowedNews[] = $allowedNews;
            $allowedNews->addAllowedGroup($this);
        }

        return $this;
    }

    public function removeAllowedNews(News $allowedNews): self
    {
        if ($this->allowedNews->removeElement($allowedNews)) {
            $allowedNews->removeAllowedGroup($this);
        }

        return $this;
    }

    public function getShortName(): ?string
    {
        return $this->shortName;
    }

    public function setShortName(string $shortName): self
    {
        $this->shortName = $shortName;

        return $this;
    }

    public function getMotd(): ?string
    {
        return $this->motd;
    }

    public function setMotd(?string $motd): self
    {
        $this->motd = $motd;

        return $this;
    }

    public function getMotdUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->motdUpdatedAt;
    }

    public function setMotdUpdatedAt(?\DateTimeImmutable $motdUpdatedAt): self
    {
        $this->motdUpdatedAt = $motdUpdatedAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeImmutable $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection<int, Calendar>
     */
    public function getCalendars(): Collection
    {
        return $this->calendars;
    }

    public function addCalendar(Calendar $calendar): self
    {
        if (!$this->calendars->contains($calendar)) {
            $this->calendars[] = $calendar;
            $calendar->setMainGroup($this);
        }

        return $this;
    }

    public function removeCalendar(Calendar $calendar): self
    {
        if ($this->calendars->removeElement($calendar)) {
            // set the owning side to null (unless already changed)
            if ($calendar->getMainGroup() === $this) {
                $calendar->setMainGroup(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Calendar>
     */
    public function getAllowedCalendars(): Collection
    {
        return $this->allowedCalendars;
    }

    public function addAllowedCalendar(Calendar $allowedCalendar): self
    {
        if (!$this->allowedCalendars->contains($allowedCalendar)) {
            $this->allowedCalendars[] = $allowedCalendar;
            $allowedCalendar->addAllowedGroup($this);
        }

        return $this;
    }

    public function removeAllowedCalendar(Calendar $allowedCalendar): self
    {
        if ($this->allowedCalendars->removeElement($allowedCalendar)) {
            $allowedCalendar->removeAllowedGroup($this);
        }

        return $this;
    }

    /**
     * @return Collection<int, SubGroup>
     */
    public function getSubGroups(): Collection
    {
        return $this->subGroups;
    }

    public function addSubGroup(SubGroup $subGroup): self
    {
        if (!$this->subGroups->contains($subGroup)) {
            $this->subGroups[] = $subGroup;
            $subGroup->setMainGroup($this);
        }

        return $this;
    }

    public function removeSubGroup(SubGroup $subGroup): self
    {
        if ($this->subGroups->removeElement($subGroup)) {
            // set the owning side to null (unless already changed)
            if ($subGroup->getMainGroup() === $this) {
                $subGroup->setMainGroup(null);
            }
        }

        return $this;
    }
}
