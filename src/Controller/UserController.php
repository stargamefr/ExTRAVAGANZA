<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SearchBarType;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/user', name: 'user_')]
class UserController extends AbstractController
{
    #[Route('/', name: 'list')]
    public function list(PaginatorInterface $paginator, Request $request, UserRepository $UserRepository): Response
    {
        $searchForm = $this->createForm(SearchBarType::class);
        $searchForm->handleRequest($request);

        $pagination = $paginator->paginate(
            $UserRepository->getAll()
                ->search(
                    (
                                $searchForm->isSubmitted()
                                && $searchForm->isValid()
                                && $searchForm->getData()['subject'] !== null
                            ) ? $searchForm->getData()['subject'] : null
                )
                ->onlyValid()
                ->getResult(),
            $request->query->getInt('page', 1)
        );

        return $this->render('user/list.html.twig', [
            'controller_name' => 'UserController',
            'pagination' => $pagination,
            'searchForm' => $searchForm->createView()
        ]);
    }

    #[Route('/view/{id}', name: 'view')]
    public function view(User $User): Response
    {
        return $this->render('user/view.html.twig', [
            'controller_name' => 'UserController',
            'user' => $User
        ]);
    }

    #[Route('/view/{id}/news', name: 'view_news')]
    public function viewNews(PaginatorInterface $paginator, User $User, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $User->getNews(),
            $request->query->getInt('page', 1)
        );
        return $this->render('user/news.html.twig', [
            'controller_name' => 'UserController',
            'user' => $User,
            'pagination' => $pagination
        ]);
    }

    #[Route('/view/{id}/calendars', name: 'view_calendars')]
    public function viewCalendars(PaginatorInterface $paginator, User $User, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $User->getCalendars(),
            $request->query->getInt('page', 1)
        );
        return $this->render('user/calendars.html.twig', [
            'controller_name' => 'UserController',
            'user' => $User,
            'pagination' => $pagination
        ]);
    }
}
