<?php

namespace App\Twig;

use Twig\TwigFunction;
use Twig\Extension\AbstractExtension;

class VersionExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('extravaganza_version', [$this, 'extravaganzaVersion']),
        ];
    }

    public function extravaganzaVersion()
    {
        return json_decode(file_get_contents('../package.json'))->version;
    }
}
