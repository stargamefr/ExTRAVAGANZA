<?php

namespace App\Twig;

use Twig\TwigFilter;
use Twig\Extension\AbstractExtension;

class ControllerInfosExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [

            new TwigFilter('extractControllerName', [$this, 'extractControllerName']),
            new TwigFilter('extractActionName', [$this, 'extractActionName']),
        ];
    }

    /**
    * Get current controller name
    */
    public function extractControllerName(string $value)
    {
            $pattern = "#Controller\\\([a-zA-Z]*)Controller#";
            $matches = array();
            preg_match($pattern, $value, $matches);
            return $matches[1] ?? '';
    }

    /**
    * Get current action name
    */
    public function extractActionName(string $value)
    {

            $pattern = "#::([a-zA-Z]*)#";
            $matches = array();
            preg_match($pattern, $value, $matches);

            return $matches[1];
    }
}
