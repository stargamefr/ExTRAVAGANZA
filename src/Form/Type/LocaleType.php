<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class LocaleType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'attr' => ['placeholder' => 'form_placeholder_locale'],
            'help' => 'form_help_locale',
            'label' => false,
            'required' => true,
            'choices' => [
                'form_choice_default' => null,
                'form_choice_english' => 'en',
                'form_choice_french' => 'fr',
                'form_choice_italian' => 'it',
                'form_choice_spanish' => 'es',
                'form_choice_portuguese' => 'pt',
                'form_choice_russian' => 'ru'
            ],
        ]);
    }

    public function getParent(): string
    {
        return ChoiceType::class;
    }
}
