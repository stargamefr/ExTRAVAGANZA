<?php

namespace App\DataFixtures;

use App\Entity\Group;
use App\Entity\Rank;
use App\Entity\SubGroup;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class Groups extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $groups = [
            [
                'name' => 'ExTRAVAGANZA',
                'shortname' => 'EXVGZA',
                'ranks' => [
                    [
                        'name' => 'Manager',
                        'shortname' => 'Man',
                        'power' => 999
                    ],
                    [
                        'name' => 'DJs',
                        'shortname' => 'DJs',
                        'power' => 111
                    ],
                ],
                'subgroups' => [
                    [
                        'name' => 'Resident',
                        'shortname' => 'RES'
                    ],
                    [
                        'name' => 'Guest',
                        'shortname' => 'GST'
                    ],
                ],
            ],
        ];

        foreach ($groups as $key => $group) {
            $NewGroup = new Group();
            $NewGroup->setName($group['name']);
            $NewGroup->setShortName($group['shortname']);
            foreach ($group['ranks'] as $key => $rank) {
                $newRank = new Rank();
                $newRank->setName($rank['name']);
                $newRank->setShortName($rank['shortname']);
                $newRank->setPower($rank['power']);
                $manager->persist($newRank);
                $NewGroup->addRank($newRank);
            }

            if (array_key_exists('subgroups', $group)) :
                foreach ($group['subgroups'] as $key => $subGroup) {
                    $newSubGroup = new SubGroup();
                    $newSubGroup->setName($subGroup['name']);
                    $newSubGroup->setShortName($subGroup['shortname']);
                    $manager->persist($newSubGroup);
                    $NewGroup->addSubGroup($newSubGroup);
                }
            endif;

            $manager->persist($NewGroup);
        }

        $manager->flush();
    }
}